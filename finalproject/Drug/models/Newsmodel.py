from django.db import models

from django.utils import timezone

class NewsList(models.Model):
  title=models.CharField(max_length=200)
  link=models.URLField(max_length=200)
  def __str__(self):
    return self.Newstitle